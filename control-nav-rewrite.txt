<?php
	if($_GET['section']== 'penjual' ){
		echo '
		<nav class="navbar navbar-default">
		  <div class="container-fluid">
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle" 
			  data-toggle="collapse" data-target="#menu-bakul">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span> 
			  </button>
			  <a class="navbar-brand" href="#">Bu Kartika</a>
			</div>
			<div class="collapse navbar-collapse" id="menu-bakul">
				<ul class="nav navbar-nav">
				  <li><a href="'.$url_base.'penjual/barang">Barang</a></li>
				  <li><a href="'.$url_base.'penjual/pelanggan">Pelanggan</a></li>
				  <li><a href="'.$url_base.'penjual/pesanan">Pesanan</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
				  <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Keluar</a></li>
				</ul>
			</div>
		  </div>
		</nav>
		';
	}else{
		echo '
		<nav class="navbar navbar-inverse">
		  <div class="container-fluid">
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle" 
			  data-toggle="collapse" data-target="#menu-klien">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span> 
			  </button>
			  <a class="navbar-brand" href="#">Pelanggan</a>
			</div>
			  <div id="menu-klien" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
				  <li><a href="'.$url_base.'pembeli/barang">Barang</a></li>
				  <li><a href="'.$url_base.'pembeli/keranjang">Keranjangku</a></li>
				  <li><a href="'.$url_base.'pembeli/pesanan">Riwayat Pesanan</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
				  <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Keluar</a></li>
				</ul>
			</div>
		  </div>
		</nav>
		';		
	}
?>
