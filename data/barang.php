<?php
header("Access-Control-Allow-Origin: *");
require("../kontrol/class.penjual.php");
$bakul = new penjual();
$barang = $bakul->daftarBarang('namaBarang',0,1);
$dataBarang = array();
for($i = 0 ; $i < COUNT($barang) ; $i++ ){
  $data = array(
    'kodeBarang'=>$barang[$i]['kodeBarang'],
    'namaBarang'=>$barang[$i]['namaBarang'],
    'satuanJual'=>$barang[$i]['satuanJual'],
    'hargaBarang'=>number_format($barang[$i]['hargaBarang'],0,',','.')
  );
  array_push($dataBarang,$data);
}
echo json_encode($dataBarang);
?>
