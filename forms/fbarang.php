<?php
	require("../kontrok/class.penjual.php");
	$bakul = new penjual();
	if(!$_GET['id']){
		$kodeBarang = '';
		$namaBarang = '';
		$satuanJual = '';
		$hargaBarang= '';
		$modusForm  = 'tambah';
		
	}else{
		$kodeBarang = $_GET['id'];
		$data = $bakul->daftarBarang('namaBarang',0,"kodeBarang='$kodeBarang'");
		$namaBarang = $data[0]['namaBarang'];
		$satuanJual = $data[0]['satuanJual'];
		$hargaBarang= $data[0]['hargaBarang'];
		$modusForm  = 'ganti';
	}
?>

<form action="aksi/barang.php" method="post" class="form-horizontal">
	<input type="hidden" name="formo" value="<?php echo $modusForm; ?>" />
	<div class="form-group">
		<label class="col-sm-3">Kode Barang</label>
		<div class="col-sm-9">
			<input class="form-control" name="kodeBarang" readonly
			value="<?php echo $kodeBarang ; ?>" />
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-3">Nama Barang</label>
		<div class="col-sm-9">
			<input class="form-control" name="namaBarang" 
			value="<?php echo $namaBarang ; ?>" />
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-3">Satuan Jual</label>
		<div class="col-sm-9">
			<input class="form-control" name="satuanJual" 
			value="<?php echo $satuanJual ; ?>" />
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-3">Harga Barang</label>
		<div class="col-sm-9">
			<input class="form-control" name="hargaBarang" 
			value="<?php echo $hargaBarang ; ?>" />
		</div>
	</div>
	<div class="form-group" style="text-align: right; padding-right: 30px;">
		<input type="submit" value="Simpan" class="btn btn-primary" />
	</div>
</form>
