<?php
	require("../kontrok/class.penjual.php");
	$bakul = new penjual();
	if(!$_GET['id']){
		$nomorPelanggan = '';
		$namaPelanggan  = '';
		$modusForm  	= 'tambah';
		
	}else{
		$nomorPelanggan = $_GET['id'];
		$data = $bakul->daftarPelanggan(0,'namaPlanggan',"noHP='$nomorPelanggan'");
		$namaPelanggan = $data[0]['namaPelanggan'];
		$modusForm = 'ganti';
	}
?>

<form action="aksi/pelanggan.php" method="post" class="form-horizontal">
	<input type="hidden" name="formo" value="<?php echo $modusForm; ?>" />
	<div class="form-group">
		<label class="col-sm-3">Nomor HP</label>
		<div class="col-sm-9">
			<input class="form-control" name="noHP" readonly
			value="<?php echo $nomorPelanggan ; ?>" />
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-3">Nama Pelanggan</label>
		<div class="col-sm-9">
			<input class="form-control" name="namaPelanggan" 
			value="<?php echo $namaPelanggan ; ?>" />
		</div>
	</div>
	
	<div class="form-group" style="text-align: right; padding-right: 30px;">
		<input type="submit" value="Simpan" class="btn btn-primary" />
	</div>
</form>
