<!-- tanpa url rewrite -->
<?php if(!$_GET){ header("Location:./?section=penjual&p=1"); } ?>
<!-- dengan url rewrite -->
<?php
/*
  header("Access-Control-Allow-Origin:*");
	if(!$_GET){ header("Location:./penjual"); }
	$url_base = $_SERVER['HOST'].'/kiosebelah/';
*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>KIOS SEBELAH</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://localhost/ngh/kiosebelah/css/bootstrap.min.css">
  <script src="http://localhost/ngh/kiosebelah/js/jquery.min.js"></script>
  <script src="http://localhost/ngh/kiosebelah/js/bootstrap.min.js"></script>
  <!-- custom css -->
  <style>
  nav > div > ul > li.active > a { background: #EEF; }
  .navbar-inverse .navbar-brand,
  .navbar-inverse .navbar-nav > li > a {
    color: #EEE;
  }
  .navbar-inverse .navbar-nav > li > a:hover {
    font-weight: bold;
  }
  #konten-utama {min-height: 400px;}
  footer {
	text-align: center; font-family:monospace; font-size: 10px;
	padding: 20px; margin-bottom: -20px;
	background: #DDD; color: #777;
  }
  </style>
	<script>
	var urlBase="http://likgopar.mugeno.org/kiosebelah/"
	</script>
</head>
<body>
  <div class="container">
	  <!-- jumbotron / header halaman -->
	  <div class="page-header">
	    <h1>Kios Mbak Kartika</h1>
	  </div>

	  <!-- navigasi -->
	  <?php include "control-nav.php"; ?>

	  <!-- konten utama -->
	  <div class="row" id="konten-utama">
		<?php include "control-konten.php"; ?>
	  </div>


	  <!-- footer -->
	  <div class="row">
		<footer>SKB Banjarnegara &copy; November 2018</footer>
	  </div>
  </div>
</body>
</html>
