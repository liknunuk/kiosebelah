<?php
	require("class.db.php");
	
	class pembeli extends db
	{
		//kelola pesanan
		function pesananBaru($noHP){
			$this->simpan("pesananInfo","noHP","'$noHP'");
		}
		
		function cariBarang($kondisi,$urut){
			$barang = $this->tampil("*","barang",$kondisi,$urut,0);
			return $barang;
		}
		
		function tambahBarangPesanan($nomorpesanan,$kodeBarang,$jumlahBarang){
			$harga = $this->hargaBarang($kodeBarang);
			$jumlahHarga = $harga * $jumlahBarang;
			
			$kolom="nomorpesanan,kodeBarang,jumlahBarang,hargaBarang,jumlahHarga";
			$data ="'$nomorpesanan','$kodeBarang','$jumlahBarang','$harga','$jumlahharga'";
			$this->simpan("pesananData",$kolom,$data);
		}
		
		function hargaBarang($kodeBarang){
			$harga = $this->tampil("hargaBarang","barang","kodeBarang='$kodeBarang'","hargaBarang",0);
			return $harga['hargaBarang'];
		}
		
		function gantiDataPesanan($kodeBarang,$jumlah,$idxpes){
			$dataset = "kodeBarang='$kodeBarang', jumlahBarang='$jumlah'";
			$kondisi = "indexPesanan = '$idxpes'";
			$this->update("pesananData",$dataset,$kondisi);
		}
		
		function batalPesanan($kodePesanan){
			$this->update("pesananInfo","status='batal'","nomorPesanan='$kodePesanan'");
		}
		
		function nomorPesananTerakhir($noHP){
			$nopes = $this->tampil("max(nomorPesanan) nopes","pesanan","noHP='$noHP'","nomorPesanan Desc",0);
			return $nopes['nopes'];
		}
		
		function dataPesananTerakhir($noHP){
			$kodePesanan = $this->nomorPesananTerakhir($noHP);
			$data = $this->tampil("pesanan.*, barang.namaBarang",
					"pesananData, barang","nomorPesanan='$kodePesanan' &&
					 barang.kodeBarang = pesananData.kodeBarang ",
					 "barang.namaBarang");
			return($data);
		}
	}

?>
