CREATE DATABASE kiosebelah;

DROP TABLE IF EXISTS barang;
CREATE TABLE barang(
   kodeBarang int(4) unsigned zerofill auto_increment,
   namaBarang varchar(30),
   satuanJual varchar(10),
   hargaBarang int(6),
   primary key(kodeBarang)
);

-- data barang contoh --
INSERT INTO barang VALUES
('0001','Kangkung','ikat',3500),
('0002','Caysim','ikat',2000),
('0004','Beras','Kg',11000),
('0005','Terigu','Kg',8000),
('0006','Minyak','liter',12000),
('0007','Gula Pasir','Kg',11500),
('0008','Tahu Putih','Bks',3500),
('0009','Tahu Kuning','Bks',3500),
('0010','Tahu Kulit','ikat',3000),
('0011','Tempe Daun','Biji',250),
('0012','Tempe Lontrong','Biji',1500);


DROP TABLE IF EXISTS pelanggan;
CREATE TABLE pelanggan(
  noHP varchar(12) unique,
  namaPelanggan varchar(30),
  primary key(noHP)
);

-- data pelanggan contoh --
INSERT INTO pelanggan VALUES
('08112345678','Bu Adeliah'),
('08122345678','Bu Barokah'),
('08132345678','Bu Cerigis'),
('08142345678','Bu Delimah'),
('08152345678','Bu Emiliah'),
('08162345678','Bu Fatimah'),
('08172345678','Bu Giselah'),
('08182345678','Bu Hamidah'),
('08192345678','Bu Idaiyah'),
('08102345678','Bu Julekah');

DROP TABLE IF EXISTS pesananInfo;
CREATE TABLE pesananInfo(
	nomorPesanan int(6) unsigned zerofill auto_increment,
	tanggalPesan timestamp default current_timestamp(),
	noHP varchar(12) not null,
	statusPesanan enum('tunggu','proses','selesai','lunas','batal') default 'tunggu',
	primary key(nomorPesanan)
);

DROP TABLE IF EXISTS pesananData;
CREATE TABLE pesananData(
	indexPesanan int(6) unsigned zerofill auto_increment,
	nomorPesanan int(6) not null,
	kodeBarang int(4) not null,
	hargaBarang int(6) not null,
	jumlahBarang float(2,2), check(jumlahBarang <= 10),
	jumlahHarga int(6) not null,
	primary key(indexPesanan)
);

