<!DOCTYPE html>
<html lang="en">
<head>
  <title>KIOS SEBELAH</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://localhost/ngh/kiosebelah/css/bootstrap.min.css">
  <script src="http://localhost/ngh/kiosebelah/js/jquery.min.js"></script>
  <script src="http://localhost/ngh/kiosebelah/js/bootstrap.min.js"></script>
  <!-- custom css -->
  <style>

  #konten-utama {min-height: 400px;}
  footer {
	text-align: center; font-family:monospace; font-size: 10px;
	padding: 20px; margin-bottom: -20px;
	background: #DDD; color: #777;
  }
  </style>

	<script>
	var urlBase="http://likgopar.mugeno.org/kiosebelah/"
  $(document).ready( function(){
    var kota = $("#kota").val();
    $.ajax({
      url:"supplyer-data.php",
      success: function(supplyers){
        $("#supplyerData").html(supplyers);
      }
    });

    $("#kota").change( function(){
      var kota = $("#kota").val();
      $.ajax({
        url:"supplyer-data.php?kota="+kota,
        success: function(supplyers){
          $("#supplyerData").html(supplyers);
        }
      })
    });
  });
	</script>
</head>
<body>
  <div class="container">
	  <!-- jumbotron / header halaman -->
	  <div class="page-header">
	    <h1>Kios Mbak Kartika</h1>
	  </div>
    <div class="row" id="konten-utama">
      <h1>DAFTAR SUPPLYER</h1>
      <div>
        Pilih Kota:
        <select id="kota">
          <option>Banjarnegara</option>
          <option>Purbalingga</option>
          <option>Wonosobo</option>
        </select>
      </div>
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Nomor</th>
            <th>Nama</th>
            <th>Alamat</th>
            <th>Telepon</th>
          </tr>
        </thead>
        <tbody id="supplyerData">

        </tbody>
      </table>
    </div>


	</div>


	  <!-- footer -->
	  <div class="row">
		<footer>SKB Banjarnegara &copy; November 2018</footer>
	  </div>
  </div>
</body>
</html>
